import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

const MoviesList = () => import(/* webpackChunkName: "MoviesList" */ './components/MoviesList.vue')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: 'movies-list',
      children: [
        {
          path: '/movies-list',
          name: 'movies-list',
          component: MoviesList
        }
      ]
    }

  ]

})
